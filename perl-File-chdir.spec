%global _empty_manifest_terminate_build 0
Name:                perl-File-chdir
Version:             0.1011
Release:             3
Summary:             More sensible way to change directories
License:             GPL+ or Artistic
URL:                 http://search.cpan.org/dist/File-chdir/
Source0:             http://www.cpan.org/authors/id/D/DA/DAGOLDEN/File-chdir-%{version}.tar.gz
BuildRoot:           %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:           noarch
BuildRequires:       perl-generators
BuildRequires:       perl >= 0:5.006 perl(Carp) perl(Cwd) >= 3.16 perl(Exporter)
BuildRequires:       perl(ExtUtils::MakeMaker) perl(File::Spec) perl(File::Spec::Functions) >= 3.27
BuildRequires:       perl(strict) perl(Test::More) perl(warnings)
Requires:            perl(Carp) perl(Cwd) >= 3.16 perl(Exporter) perl(File::Spec::Functions) >= 3.27
Requires:            perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version)) perl(strict)

%description
Perl's chdir() has the unfortunate problem of being very, very, very
global. If any part of your program calls chdir() or if any library you
use calls chdir(), it changes the current working directory for the
*whole* program.

%package help
Summary:             More sensible way to change directories
Provides:            perl-File-chdir-doc

%description help
Perl's chdir() has the unfortunate problem of being very, very, very
global. If any part of your program calls chdir() or if any library you
use calls chdir(), it changes the current working directory for the
*whole* program.

%prep
%setup -q -n File-chdir-%{version}

%build
export PERL_MM_OPT=""
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
export PERL_MM_OPT=""
rm -rf $RPM_BUILD_ROOT
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;
%{_fixperms} $RPM_BUILD_ROOT/*
pushd %{buildroot}
touch filelist.lst
if [ -d usr/bin ];then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ];then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .

%check
make test
%clean
rm -rf $RPM_BUILD_ROOT

%files -f filelist.lst
%defattr(-,root,root,-)
%doc Changes CONTRIBUTING.mkdn cpanfile dist.ini LICENSE META.json perlcritic.rc README
%{perl_vendorlib}/*

%files help
%{_mandir}/*

%changelog
* Wed Aug 5 2020 dingyue <dingyue5@huawei.com> - 0.1011-3
- Fix the package dependency.

* Fri Jul 10 2020 chengzihan <chengzihan2@huawei.com> - 0.1011-2
- Package init
